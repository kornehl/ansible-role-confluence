# Ansible role to install Confluence

I use this role to setup local testing environments.
It can be configured to use different databases on the same or a seperate host.
Installation of proxy and SSL configuration is planned.

## Requirements

root access is required, so either run it in a playbook with a global `become: yes`, or invoke the role in your playbook like:

```
- hosts: confluence-server
  roles:
    - role: ansible-role-confluence
      become: yes
```

## Installation

If not installing to the default directory, e.g. `/etc/ansible`, don't forget the `-p`-Option:

```
  ansible-galaxy install git+https://bitbucket.org/cmbaer/ansible-role-confluence -p roles
```

## Role Variables

Available variables can be found in `defaults/main.yml`

## Dependencies

Depending on the database vendor you want to choose.


## Example Playbook

    - hosts: confluence-server
      become: yes
      vars_files:
        - vars/main.yml
      roles:
        - { role: confluence-ansible-role }

## License

MIT / BSD

## Author Information

This role was created in 2019 by [Christian Baer](https://bitbucket.org/cmbaer/)
